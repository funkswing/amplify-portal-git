from django.http import HttpResponse
from django.core import serializers
from people.models import Person


def get_all_persons(request):
    """
    Return JSON from Django QuerySet DB query
    :param request: request
    :return: HttpResponse
    """

    json_serializer = serializers.get_serializer("json")()
    response = json_serializer.serialize(
                Person.objects.all(),
                ensure_ascii=False,
                indent=2,
                use_natural_keys=True)
    return HttpResponse(response, mimetype="application/json")
