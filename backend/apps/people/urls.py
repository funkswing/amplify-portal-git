from django.conf.urls import patterns, url
from people import views


urlpatterns = patterns('',
    url(r'^$', views.get_all_persons),
)
