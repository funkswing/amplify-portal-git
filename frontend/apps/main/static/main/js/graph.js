$( document ).ready(function() {

    $.get("/people", function (data) {
        drawGraph(JSON.parse(data));
    });

    function checkUnique (value, index, self) {
        // Check if this is first occurrence of item in array. If so, return `true`.
        // `array.indexOf(item)` returns only the first index of item found in array;
        // therefore, for each occurrence after the first this evaluates to `false`
        return self.indexOf(value) === index;
    }

    function drawGraph (data) {
        // Extract car makes from JSON response data
        var car_makes = data.map(function (i) {
            return i.fields.car_make;
        });

        // Create array of only unique car makes
        var uniqueCarMakes = car_makes.filter(checkUnique);

        // Log data to console
        console.log(car_makes);
        console.log(uniqueCarMakes);

        var carMakesCount = [];
        for (i = 0; i<uniqueCarMakes.length; i++) {
            var carMake = uniqueCarMakes[i];
            var countMakes = car_makes.filter(function(val) {
                return val === carMake;
            });
            carMakesCount.push(countMakes.length);
        }

        // Create data object to pass to Chart constructor
        var chartData = {
            labels: uniqueCarMakes.sort(),
            datasets: [
                {
                    label: "Frequency of Car Makes",
                    data: carMakesCount,
                    backgroundColor: [
                        "#F8CBC8",
                        "#F97B6A",
                        "#90A8D0",
                        "#035185",
                        "#FBDD3A",
                        "#96DCDE",
                        "#9897A4",
                        "#B28E6A",
                        "#76C453",
                        "#DF4831"
                    ],
                    borderWidth: 1
                }
            ]
        };

        // DOM canvas element to hold chart
        var ctx = $('#amp_chart');

        // Construct Chart
        new Chart(ctx, {
            type: "bar",
            data: chartData,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    }

});

