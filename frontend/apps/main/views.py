from django.http import HttpResponse
from django.shortcuts import render
import requests


_REST_HOST = "http://127.0.0.1:8500"


def show_homepage(request):
    return render(request, 'main/main.html')


def get_people(request):

    url = "{}/people".format(_REST_HOST)
    response = requests.get(url)

    return HttpResponse(response.content)
